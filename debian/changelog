libnet-smpp-perl (1.19-2) unstable; urgency=medium

  * Team upload

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * debian/control: remove Nicholas Bamber from Uploaders on request of
    the MIA team.
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jose Luis Rivas from Uploaders. Thanks for your work!

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Florian Schlichting ]
  * Add spelling.patch
  * Bump dh compat to level 11
  * Declare compliance with Debian Policy 4.1.5

 -- Florian Schlichting <fsfs@debian.org>  Fri, 06 Jul 2018 22:17:24 +0200

libnet-smpp-perl (1.19-1) unstable; urgency=low

  [ Nicholas Bamber ]
  * New upstream release
  * Raised standards version to 3.9.2

  [ gregor herrmann ]
  * Update years of upstream and packaging copyright.

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Sat, 11 Jun 2011 10:55:26 +0100

libnet-smpp-perl (1.18-1) unstable; urgency=low

  * Adding myself to Uploaders
  * New upstream release
  * Upped standards version to 3.9.1
  * Checked dependencies
  * Refreshed copyright

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Fri, 31 Dec 2010 13:10:25 +0000

libnet-smpp-perl (1.14-1) unstable; urgency=low

  * New upstream release.
    + Removed applied fix-FTBFS-missing-bracket.patch patch.
    + Removed applied 01_wrong-path-for-interpreter.patch patch.
  * Bumped up Standards-Version to 3.9.0 (no changes).
  * Refreshed years for copyright holders in d.copyright.
  * Added /me as Uploaders (Updated both d.control and d.copyright).

 -- Franck Joncourt <franck@debian.org>  Tue, 06 Jul 2010 23:00:47 +0200

libnet-smpp-perl (1.13-1) unstable; urgency=low

  [ gregor herrmann ]
  * New upstream release.
  * Refresh patch.
  * debian/copyright: update formatting and years of upstream copyright.
  * Convert to source format 3.0 (quilt).
  * Add /me to Uploaders.
  * Set Standards-Version to 3.8.4 (no changes).
  * debian/control: lowecase short description, mention module name in long
    description.

  [ Salvatore Bonaccorso ]
  * Add fix-FTBFS-missing-bracket.patch to prevent FTBFS due to a missing
    bracket in SMPP.pm.
  * debian/control: Remove Priority and Section fields from the binary package
    stanza.

 -- gregor herrmann <gregoa@debian.org>  Sat, 06 Mar 2010 18:02:05 +0100

libnet-smpp-perl (1.12-1) unstable; urgency=low

  * New upstream release (Closes: #329638)
  * Added debian/watch file.
  * Added debian/libnet-smpp-perl.{docs,examples}
  * Upgraded debian/rules to dh7 and upgraded dependencies on debhelper as
    well as debian/compat and added override on debian/rules to remove *.pl
    scripts that are examples.
  * Updated debian/copyright.
  * Added Debian Perl Group as Maintainer and added me as uploader. (Closes:
    #470585)
  * Bumped Standards-Version to 3.8.3, thus removed versioned dependency on
    perl >= 5.6.0-16.
  * Removed name of the package on short-description and update
    long-description.
  * Add debian/README.source to document quilt usage, as required by
    Debian Policy since 3.8.0.
  * Added quilt and patches to fix bad path to perl on some scripts.
  * Added Vcs-* and Homepage fields on debian/control.

 -- Jose Luis Rivas <ghostbar@debian.org>  Sat, 03 Oct 2009 15:37:25 -0430

libnet-smpp-perl (1.03-1) unstable; urgency=low

  * Initial debianization. Closes: #277721

 -- Andres Seco Hernandez <AndresSH@debian.org>  Fri, 22 Oct 2004 01:37:11 +0200
